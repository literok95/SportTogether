## Пиццафабрика 13.12.0 (2021-05-22)

### Security (3 changes)

- Prevent DOS from Chaining in Mermaid. !60382
- Report pipeline creation success only when warranted. !60746
- Fix XSS vulnerability in shared runner description. !60891
